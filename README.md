### deseqAbstraction
 - Object-oriented analysis of RNA-seq data in R! 
 - For standard deseq pipelines - made really easy and comfortable.
 - deseqAbs object to ease every analysis, handling and visualization of RNA-seq data
 - Based on DESeq2
 - <b>Main usage tutorial: Usage.pdf or Usage.html in deseqAbstraction (current) directory. </b>
 - More usage tutorials found in usageTutorials/deseqAbs repository!



